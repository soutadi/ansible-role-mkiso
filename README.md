Role Name
=========

An Ansible role to create OS ISO images.

Requirements
------------

No specific requirements.

Role Variables
--------------

```Yaml
# The linux distribution
# centos
mkiso_distribution: centos

# The linux distribution version
mkiso_distribution_version: 7

# The workspace directory for the role
mkiso_iso_dir: "/tmp/boot_iso"

# Path of the iso file
mkiso_iso_path: "{{ mkiso_iso_dir }}/{{ mkiso_ks_hostname }}_{{ mkiso_distribution }}.iso"

# bios or uefi
mkiso_boot_mode: bios

# Template to use for kickstart
mkiso_ks_file_template: ks.cfg.j2

# Template to use for paritioning in kickstart
mkiso_ks_partitions_template: ks_partitions.j2

# Template to use for packages in kickstart
mkiso_ks_packages_template: ks_packages.j2

# Template to use for custom content in kickstart
mkiso_ks_custom_content_template: ks_custom_content.j2

# Template to use for repositories in kickstart
mkiso_ks_repos_template: ks_repos.j2

# Kickstart configuration
mkiso_ks_auth: --enableshadow --passalgo=sha512

mkiso_ks_firstboot: --enable

mkiso_ks_action_after_successful_installation: shutdown

# By default all partitions on /dev/sda will be removed.
mkiso_ks_disk_device: sda

mkiso_ks_timezone: Europe/Stockholm --isUtc

mkiso_ks_keyboard: --vckeymap=us --xlayouts='us'

mkiso_ks_lang: en_US.UTF-8

mkiso_ks_ignore_disk: --only-use={{ mkiso_ks_disk_device }}

mkiso_ks_bootloader: --append=" crashkernel=auto" --location=mbr --boot-drive={{ mkiso_ks_disk_device }}

mkiso_ks_services: --enabled="chronyd"

mkiso_ks_root_password: 123

# Network configuration
# static or dhcp
mkiso_ks_bootproto: static

# FQDN
mkiso_ks_hostname: vm1.example.com

# Network configuration in case of static bootproto
mkiso_ks_ip: 192.168.122.50
mkiso_ks_netmask: 255.255.255.0
mkiso_ks_gateway: 192.168.122.1
mkiso_ks_name_server: 8.8.8.8

# Extra repositories to add
# Example:
# mkiso_ks_extra_repositories:
#   - name: epel
#     url: https://dl.fedoraproject.org/pub/epel/7/x86_64/
mkiso_ks_extra_repositories: []

# List of packages to install
mkiso_packages_to_install:
  - chrony
  - kexec-tools

# Generate and populate ssh key in authorized_keys as part of installation
mkiso_populate_ssh_key: true

# Populate ssh key in authorized_keys for this user
mkiso_ssh_user: root

# SSH Identity file to be used for ansible user. It will be created if does not exist.
mkiso_ssh_identity_file_path: "{{ lookup('env','HOME') }}/.ssh/id_rsa_ansible"

```

Dependencies
------------

N/A

Example Playbook
----------------

BIOS only with static networking
```Yaml
- name: Create boot iso
  hosts: localhost
  vars:
    mkiso_ks_hostname: vm1
    mkiso_ks_ip: 192.168.0.100
    mkiso_ks_netmask: 255.255.255.0
    mkiso_ks_gateway: 192.168.0.1
    mkiso_ks_name_server: 192.168.0.1
  roles:
    - role: siavashoutadi.mkiso
```
UEFI with DHCP networking
```Yaml
- name: Create boot iso
  hosts: localhost
  vars:
    mkiso_boot_mode: uefi
    mkiso_ks_hostname: vm1
    mkiso_ks_bootproto: dhcp
  roles:
    - role: siavashoutadi.mkiso
```

License
-------

Apache 2.0
